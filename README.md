# LifeWorks Project

This project adheres to the acceptance criteria outlined [here](https://bitbucket.org/FazAmman/lifeworkstest/src/master/serverless_code_test.pdf) with some additional functionality for future works.

# Development server

- Run `npm i` to install dependencies
- Run `npm test` to ensure server runs as expected with TS
- Run `npm start` to start the server

# Usage

- Once the server is started you should recieve the message "server started at http://localhost:PORT" where PORT = /.env SERVER_PORT
- The API usage is very open and allows additional functionality to sort / filter items. Cats are grouped and ordered as specified and the animals are in the required order.

- Retrieve animals WITHOUT any sorting (only ordered by animals dogs->cats->hamsters) [click here](http://localhost:8080/api/animals)
- Retrieve animals as per AC [click here](http://localhost:8080/api/animals?dogs=[{%22field%22:%22age%22,%22sort%22:%22DSC%22}]&hamsters=[{%22field%22:%22age%22,%22sort%22:%22ASC%22}])
- Retrieve animals with dogs sorted by forename DSC, cats sorted by fullname ASC and hamsters sorted by age ASC [click here](http://localhost:8080/api/animals?dogs=[{%22field%22:%22forename%22,%22sort%22:%22DSC%22}]&cats=[{%22field%22:%20%22fullname%22,%22sort%22:%20%22ASC%22}]&hamsters=[{%22field%22:%22age%22,%22sort%22:%22ASC%22}]) 

- The api is very open to sorting based on user requirements. It can also be extended in the future to handle filtering aswell as sorting, please see comments in code.
- The user may sort by forename, surname, fullname, age or color by passing the respective parameters into the field attr. For example to filter dogs by age ascending and cats by fullname descending and hamsters by firstname ascending the query parameters would be: /api/animals?dogs=[{"field":"age","sort":"ASC"}]&cats=[{"field":"fullname","sort":"DSC"}]&hamsters=[{"field":"forename","sort":"ASC"}]

# Notes
- Ensure port 8080 is free, otherwise change the port in /.env
- Grouping and sorting of cats requires some rework to ensure it is more dynamic
- Requires unit and e2e tests
- This API was completed used NodeJS and TS (Typescript). Unfortunately unable to deploy to Lambda in its current state.

# Future work
- Add ts-watch for easier development
- Handle multiple filters
- Handle cat sorting with URL params
- Review some methods and use ES7 syntax for promises (async and await)
- Include Observables rather than promises
- Build individual endpoints for animals type possibly with some filters on specific animals aswell i.e /api/animals/cats/ginger
- Deploy to lambda by including a build script to concat, minify etc to one file.
- Use logger rather than console logs (winston)
- Enhance the get AGE to cover some month scenarios (refer to comment in code)
