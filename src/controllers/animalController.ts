import { Router } from "express";
import { IAnimalType } from "../models/animal";
import { IAnimalFields, IAnimalFilters, ISort } from "../models/filter";
import { AnimalService } from "../services/animalService";
import { allAnimalsURI } from "../utils/constants";
import { RESCUE_SHELTER_BASE_URL } from "../utils/constants";

export class AnimalController {

    public router: Router = Router();
    private service: AnimalService;

    constructor() {
        this.service = new AnimalService();
        this.init();
    }

    private init() {
        this.router.get("/", (req, res) => {
            const allAnimalURLS: string[] = [];
            allAnimalsURI.forEach((uri) => allAnimalURLS.push(`${RESCUE_SHELTER_BASE_URL}${uri}`));

            const filters: IAnimalFilters[] = this.generateFiltersFromParams(req.query);

            this.service.getAllAnimals(allAnimalURLS, filters).then((data) => {
                res.send(data);
            }).catch((err) => {
                res.status(500).send(err);
            });
        });
    }

    private generateFiltersFromParams(reqParams: any): IAnimalFilters[] {
        let filters: IAnimalFilters[] = [];
        if (reqParams) {
            if (reqParams.dogs) {
                filters = filters.concat(this.createFilter(reqParams.dogs, IAnimalType.Dog));
            }
            if (reqParams.cats) {
                filters = filters.concat(this.createFilter(reqParams.cats, IAnimalType.Cat));
            }
            if (reqParams.hamsters) {
                filters = filters.concat(this.createFilter(reqParams.hamsters, IAnimalType.Hamster));
            }
        }
        return filters;
    }

    private createFilter(params: string, animalType: number): IAnimalFilters {
        let animalFilter;
        try {
            animalFilter = JSON.parse(params);
        } catch {
            // tslint:disable-next-line:no-console
            console.error("Invalid JSON passed in URL");
        }

        animalFilter.forEach((filter: any) => {
            filter.field = IAnimalFields[filter.field];
            filter.sort === "ASC" ?  filter.sort = ISort.ASC : filter.sort = ISort.DSC;
        });

        return { type: animalType, filters: animalFilter };
    }
}
