// Below allows ease of extensibility and future enahancements / changes

export interface IRescueAnimalResponse {
    body: IAnimal[];
}

export interface IAnimal {
    forename: string;
    surname: string;
    dateOfBirth: string;
    image: IAnimalImage;
    type: IAnimalType;
}

// Inlcuded data for client as per AC - Included both fullname AND forename, lastname
export interface IAnimalClient {
    type: string;
    fullname: string;
    forename: string;
    lastname: string;
    age: IAnimalAge;
    image: string;
}

export interface IAnimalAge {
    years: number;
    months: number;
}

export interface IAnimalImage {
    url: string;
}

// tslint:disable:no-empty-interface
export interface IDog extends IAnimal {}
export interface IHamster extends IAnimal {}

export interface ICat extends IAnimal {
    colour: string;
}

// TODO
export enum ICatColour {
    ginger = 1, black = 2, other = 3
}

// Enums
export enum IAnimalType {
    Dog = 0,
    Cat = 1,
    Hamster = 2,
    Unknown = 9
}
