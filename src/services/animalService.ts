import request from "request-promise-native";
import {
    IAnimal,
    IAnimalAge,
    IAnimalClient,
    IAnimalType,
    ICat,
    ICatColour,
    IRescueAnimalResponse} from "../models/animal";
import {
    IAnimalFields,
    IAnimalFilters,
    ISort } from "../models/filter";

export class AnimalService {

    public getAllAnimals(urls: string[], filters: IAnimalFilters[]): Promise<any> {
        let failCount = 0;
        return new Promise((resolve, reject) => {
            Promise.all(urls.map((url) =>
                request.get(url)
                    .then((data) => {
                        const animalType: IAnimalType = this.getTypeFromURL(url);
                        return this.sortAnimals(filters, this.parseJSON(data, animalType));
                    })
                    .catch((error) => {
                        failCount++;
                        // tslint:disable-next-line:no-console
                        console.error("Unable to retrieve data from downstream API where URL is " + url + " and error is " + error);
                        if (failCount >= urls.length) {
                            reject("Unable to retrieve data from all downstream API calls where URLS are: " + urls);
                        }
                    })))
                .then((data) => {
                    const sortedData = this.orderAndFormatAnimals(data as Array<void | IAnimalClient[]>);
                    resolve(sortedData);
                });
        });
    }

    // TODO - make common method
    private parseJSON(data: string, animalType: IAnimalType) {
        const response = JSON.parse(data) as IRescueAnimalResponse;
        response.body.forEach((animal) => animal.type = animalType);
        return response.body;
    }

    private orderAndFormatAnimals(data: Array<void | IAnimalClient[]>) {
        // tslint:disable-next-line:curly
        if (!data) return;

        let orderedData: IAnimalClient[] = [];
        data.forEach((animalArr: IAnimalClient[]) => {
            orderedData = orderedData.concat(animalArr);
        });
        return orderedData;
    }

    private async sortAnimals(filters: IAnimalFilters[], animals: IAnimal[]) {
        let sortedAnimals: IAnimalClient[] = [];
        const animalType: IAnimalType = animals[0].type;
        const formatedAnimals: IAnimalClient[] = [];

        // TECH DEBT - Harding coding this specific sort as it requires some grouping...
        // TODO - Review this solution
        animals = this.groupAndSortCats(animals);

        animals.forEach((animal) => {
            formatedAnimals.push(this.createAnimalClient(animal));
        });

        // Default
        sortedAnimals = formatedAnimals;

        for (const filter of filters) {
            if (filter.type === animalType) {
                for (const animalFilter of filter.filters) {
                    sortedAnimals = await formatedAnimals.sort((a: any, b: any) => {
                        if (animalFilter.field === IAnimalFields.age) {
                            if (animalFilter.sort === ISort.ASC) {
                                if (a.age.years !== b.age.years) {
                                    return a.age.years - b.age.years;
                                }
                                return a.age.months - b.age.months;
                            } else {
                                if (a.age.years !== b.age.years) {
                                    return b.age.years - a.age.years;
                                }
                                return b.age.months - a.age.months;
                            }
                        }
                        // This is for future work to order STRINGS i.e via name
                        // This will work but is also dependant on the order of the filters passed in URL Params
                        // e.g. if you pass dogs=[{"field":"fullname","sort":"ASC"},{"field":"age","sort":"DSC"}]
                        // The sort will first order by name ascending THEN will order by age. This is more meant
                        // for future work to add filtering aswell as sorting where the model will have to change
                        if (animalFilter.sort === ISort.DSC) {
                            return b[IAnimalFields[animalFilter.field]]
                                .localeCompare(a[IAnimalFields[animalFilter.field]]);
                        }
                        // Default sort order is ASC
                        return a[IAnimalFields[animalFilter.field]].localeCompare(b[IAnimalFields[animalFilter.field]]);
                    });
                }
            }
        }

        return sortedAnimals;
    }

    // TECH DEBT - REVIEW THIS METHOD TO ENSURE IT MORE DYNAMIC FOR LONGEVITY
    private groupAndSortCats(animals: IAnimal[]) {
        // Default to return unsorted
        const sortedAnimals: any = [];
        const dogs: any = [];
        const hamsters: any = [];
        let sortedCats: any = [];
        let gingerCats: any = [];
        let blackCats: any = [];
        let otherColorCats: any = [];

        for (const animal of animals) {
            if (animal.type === IAnimalType.Dog) {
                dogs.push(animal);
            } else if (animal.type === IAnimalType.Cat) {
                const cat = animal as ICat;
                if (cat.colour === ICatColour[ICatColour.ginger]) {
                    gingerCats.push(cat);
                } else if (cat.colour === ICatColour[ICatColour.black]) {
                    blackCats.push(cat);
                } else {
                    otherColorCats.push(cat);
                }
            } else if (animal.type === IAnimalType.Hamster) {
                hamsters.push(animal);
            }
        }

        gingerCats = gingerCats.sort((a: ICat, b: ICat) => {
            return this.checkCatSort(a.dateOfBirth, b.dateOfBirth);
        });

        blackCats = blackCats.sort((a: ICat, b: ICat) => {
            return this.checkCatSort(a.dateOfBirth, b.dateOfBirth);
        });

        otherColorCats = otherColorCats.sort((a: ICat, b: ICat) => {
            return this.checkCatSort(a.dateOfBirth, b.dateOfBirth);
        });

        sortedCats = sortedCats.concat(gingerCats, blackCats, otherColorCats);

        return sortedAnimals.concat(dogs, sortedCats, hamsters);
    }

    private checkCatSort(a: string, b: string) {
        const catADOBInMillis = this.getDOBMillis(a);
        const catBDOBInMillis = this.getDOBMillis(b);
        return catADOBInMillis - catBDOBInMillis;
    }

    // TODO - Check date string format... use something like moment to handle this..
    // Move to utils?
    private getDOBMillis(dateString: string): number {
        return Date.parse(dateString);
    }

    // Common util method?
    private getTypeFromURL(url: string): IAnimalType {
        if (url.indexOf("dogs") > 0) {
            return IAnimalType.Dog;
        } else if (url.indexOf("cats") > 0) {
            return IAnimalType.Cat;
        } else if (url.indexOf("hamsters") > 0) {
            return IAnimalType.Hamster;
        } else {
            return IAnimalType.Unknown;
        }
    }

    // TODO - Use model for this...
    private createAnimalClient(animal: IAnimal) {
        return {
            age: this.getAnimalAge(animal.dateOfBirth),
            forename: animal.forename,
            fullname: `${animal.forename} ${animal.surname}`,
            image: animal.image.url,
            lastname: animal.surname,
            type: IAnimalType[animal.type]
        };
    }

    private getAnimalAge(dob: string) {
        // Use moment for this? Date formats may change from the service...
        const dobDate = new Date(dob);
        const today = new Date();
        let ageMonths: number;
        let ageYears: number;

        if (dobDate.getMonth() < today.getMonth()) {
            ageMonths = today.getMonth() - dobDate.getMonth();
            ageYears = today.getFullYear() - dobDate.getFullYear();
        } else {
            // TECH DEBT - Handle this scenario
            // Leaving this method for now due to data currently being returned from services
        }

        const age: IAnimalAge = {
            months: ageMonths,
            years: ageYears
        };

        return age;
    }

}
