import { equal } from "assert";
import { Logic } from "./logic";

// Boiler plate for Mocha to work with TS.
describe("Typescript usage suite", () => {
  it("should be able to execute a test", () => {
    equal(true, true);
  });

  it("should be able to run logic method", () => {
    equal(new Logic().test(), 0);
  });
});

// TODO / TECHDEBT
