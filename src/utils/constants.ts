export const RESCUE_SHELTER_BASE_URL = "https://apigateway.test.lifeworks.com/rescue-shelter-api";
export const RESCUE_SHELTER_DOG_URI = "/dogs";
export const RESCUE_SHELTER_CATS_URI = "/cats";
export const RESCUE_SHELTER_HAMSTERS_URI = "/hamsters";

// Ease of extensibility, for additional animals add the endpoint and include in the array below.
// Rest of the magic will be handled!
// This is ordered so that dogs are first, then cats then hamsters as per requirements
export const allAnimalsURI = [RESCUE_SHELTER_DOG_URI, RESCUE_SHELTER_CATS_URI, RESCUE_SHELTER_HAMSTERS_URI];
