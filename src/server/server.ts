import dotenv from "dotenv";
import express from "express";
import bodyParser from "express";
import cors from "express";
import { AnimalController } from "../controllers/animalController";

// initialize configuration
dotenv.config();

const port = process.env.SERVER_PORT; // default port to listen

const app = express();

// Setup respective libraries
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Define endpoints and respective routers
app.use("/api/animals", new AnimalController().router);

// Default test endpoints
app.get( "/", ( req, res ) => {
    res.send( "Hello world!" );
});

// start the Express server
app.listen( port, () => {
    // tslint:disable-next-line:no-console
    console.log( `server started at http://localhost:${ port }` );
});

process.on("uncaughtException", (e) => {
    // tslint:disable-next-line:no-console
    console.log(e);
    // process.exit(1);
});

process.on("unhandledRejection", (e) => {
    // tslint:disable-next-line:no-console
    console.log(e);
    // process.exit(1);
});
