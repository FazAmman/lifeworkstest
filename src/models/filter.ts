import { IAnimalType } from "./animal";

export interface IAnimalFilters {
    filters: IAnimalFilter[];
    type: IAnimalType;
}

export interface IAnimalFilter {
    field: IAnimalFields;
    sort: ISort;
}

export enum ISort {
    ASC = 0,
    DSC = 1
}

export enum IAnimalFields {
    forename = 0,
    surname = 1,
    fullname = 2,
    age = 3,
    color = 4
}
